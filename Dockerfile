FROM java:8-jre-alpine
WORKDIR /
COPY target/spring-petclinic-1.4.2.jar app
EXPOSE 8080 
CMD java -jar app
